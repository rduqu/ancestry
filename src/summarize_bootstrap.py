#!/usr/bin/python

import sys, os
from collections import defaultdict

pop2clust = dict()
clust2cont = dict()
pop2cont = dict()
conts = dict()

infile = open(sys.argv[1])
for line in infile.xreadlines():
	line = line.strip().split()
	pop = line[0]
	clust = line[1]
	cont = line[2]
	pop2clust[pop] = clust
	pop2cont[pop] = cont
	clust2cont[clust] = cont
	conts[cont] = True

NBOOT = 0
TRIVIAL_CUTOFF = 0.01
BOOTCUTOFF = 0.8

pop2fracs = defaultdict(list)
clust2fracs = defaultdict(list)
cont2fracs = defaultdict(list)

bootfiles = sys.argv[2:]
for f in bootfiles:
	infile = open(f)
	p2f = defaultdict(lambda:0)
	cl2f = defaultdict(lambda:0)
	co2f = defaultdict(lambda:0)
	for line in infile.xreadlines():
		line = line.strip().split()
		pop = line[0]
		clust = pop2clust[pop]
		cont = pop2cont[pop]
		frac = float(line[1])
                p2f[pop] = frac
                cl2f[clust] += frac
                co2f[cont] += frac
	for pop in pop2clust.keys():
                pop2fracs[pop].append(p2f[pop])
	for clust in clust2cont.keys():
                clust2fracs[clust].append(cl2f[clust])
	for cont in conts:
                cont2fracs[cont].append(co2f[cont])
	NBOOT = NBOOT+1

def mean(l):
        return sum(l)/len(l)

def enough(l):
        count = sum( [f>TRIVIAL_CUTOFF for f in l] )
        return (float(count)/float(NBOOT) > BOOTCUTOFF)

toprint = dict()
accounted = defaultdict(lambda:0)
for pop, fracs in pop2fracs.items():
        if max(fracs)-min(fracs)>0.1:
                sys.stderr.write('WARNING: High variation for %s: %s\n' % (pop, sorted(fracs)))
        if mean(fracs)>0.02 and not enough(fracs):
                sys.stderr.write('WARNING: Significant mean (%.3f) dropped for %s: %s\n' %
                                 (mean(fracs), pop, sorted(fracs)))
	if enough(fracs):
		meanf = mean(fracs)
		toprint[pop] = meanf
                accounted[pop2clust[pop]] += meanf
                accounted[pop2cont[pop]] += meanf
for clust, fracs in clust2fracs.items():
        if enough(fracs):
                meanf = mean(fracs) - accounted[clust]
                if meanf > TRIVIAL_CUTOFF:
			toprint["AMBIG_"+clust] = meanf
                        accounted[clust2cont[clust]] += meanf
for cont, fracs in cont2fracs.items():
        if enough(fracs):
                meanf = mean(fracs) - accounted[cont]
                if meanf > TRIVIAL_CUTOFF:
			toprint["AMBIG_"+cont] = meanf

total = 0
for p, f in toprint.items():
	total = total+f

ambig = 1-total
if ambig > TRIVIAL_CUTOFF:
        toprint["AMBIGUOUS"] = ambig
for l, f in toprint.items():
	print l, f
